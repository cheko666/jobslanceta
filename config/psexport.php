<?php

return [
    'email' => [
      'to'   =>  [
          env('MAIL_TO')
      ],
        'from'  => [
            env('MAIL_FROM')
        ]
    ],
    'cabeceras'  => [
        'v16' => [
            'producto_baja' =>  [
                'id'
            ],
            'producto'  =>
                [
                    'ID',
                    'Active',
                    'Name',
                    'Categories',
                    'Price',
                    'Tax_rules_ID',
                    'Wholesale_price',
                    'On_sale',
                    'Discount_amount',
                    'Discount_percent',
                    'Discount_from',
                    'Discount_to',
                    'Reference',
                    'Supplier_reference',
                    'Supplier',
                    'Manufacturer',
                    'EAN13',
                    'UPC',
                    'Ecotax',
                    'Width',
                    'Height',
                    'Depth',
                    'Weight',
                    'Quantity',
                    'Minimal_quantity',
                    'Visibility',
                    'Additional_shipping_cost',
                    'Unity',
                    'Unity_price',
                    'Short_description',
                    'Description',
                    'Tags',
                    'Meta_title',
                    'Meta_keywords',
                    'Meta_description',
                    'URL_rewritten',
                    'Text_when_in_stock',
                    'Text_when_backorder_allowed',
                    'Available_for_order',
                    'Product_available_date',
                    'Product_creation_date',
                    'Show_price',
                    'Image_URLs',
                    'Delete_existing_images',
                    'Feature',
                    'Available_online_only',
                    'AS_Condition',
                    'Customizable',
                    'Uploadable_files',
                    'Text_fields',
                    'Out_of_stock',
                    'ID_/_Name_of_shop',
                    'Advanced_stock_management',
                    'Depends_On_Stock',
                    'Warehouse'
                ],
            'combinacion'   =>  [
                'ID',
                'Product_Reference',
                'Attribute',
                'Value',
                'Supplier_reference',
                'Reference',
                'EAN13',
                'UPC',
                'Wholesale_price',
                'Impact_on_price',
                'Ecotax',
                'Quantity',
                'Minimal_quantity',
                'Impact_on_weight',
                'Default',
                'Combination_available_date',
                'Image_position',
                'Image_URL',
                'Delete_existing_images',
                'ID_/_Name_of_shop',
                'Advanced_Stock_Managment',
                'Depends_on_stock',
                'Warehouse'
            ],
            'precio-especifico' =>  [
                'id_producto',
                'referencia',
                'descuento'
            ],
            'producto-transportista'    => [
                'id_producto',
                'id_transportista'
            ],
            'marca' =>  [
                'ID',
                'Active',
                'Name',
                'Description',
                'Short description',
                'Meta title',
                'Meta keywords',
                'Meta description',
                'Image URL'
            ],
            'categoria' =>  [
                'ID',
                'Active',
                'Name',
                'Parent category',
                'Root category',
                'Description',
                'Meta title',
                'Meta keywords',
                'Meta description',
                'URL',
                'Image URL'
            ]
        ],
        'v17' => [
            'producto_baja' =>  [
                'id'
            ],

            'producto'  =>
                [
                    'ID',
                    'Active',
                    'Name',
                    'Categories',
                    'Price',
                    'Tax_rules_ID',
                    'Wholesale_price',
                    'On_sale',
                    'Discount_amount',
                    'Discount_percent',
                    'Discount_from',
                    'Discount_to',
                    'Reference',
                    'Supplier_reference',
                    'Supplier',
                    'Manufacturer',
                    'EAN13',
                    'UPC',
                    'Ecotax',
                    'Width',
                    'Height',
                    'Depth',
                    'Weight',
                    'Quantity',
                    'Minimal_quantity',
                    'Visibility',
                    'Additional_shipping_cost',
                    'Unity',
                    'Unity_price',
                    'Short_description',
                    'Description',
                    'Tags',
                    'Meta_title',
                    'Meta_keywords',
                    'Meta_description',
                    'URL_rewritten',
                    'Text_when_in_stock',
                    'Text_when_backorder_allowed',
                    'Available_for_order',
                    'Product_available_date',
                    'Product_creation_date',
                    'Show_price',
                    'Image_URLs',
                    'Image_alt',
                    'Delete_existing_images',
                    'Feature',
                    'Available_online_only',
                    'AS_Condition',
                    'Customizable',
                    'Uploadable_files',
                    'Text_fields',
                    'Out_of_stock',
                    'ID_/_Name_of_shop',
                    'Advanced_stock_management',
                    'Depends_On_Stock',
                    'Warehouse',
                    'Accesories'
                ],
/*
            'producto'  =>
                [
                    'ID',
                    'Active',
                    'Name',
                    'Categories',
                    'Price',
                    'Tax_rules_ID',
                    'Wholesale_price',
                    'On_sale',
                    'Discount_amount',
                    'Discount_percent',
                    'Discount_from',
                    'Discount_to',
                    'Reference',
                    'Supplier_reference',
                    'Supplier',
                    'Manufacturer',
                    'EAN13',
                    'UPC',
                    'NPM',
                    'Ecotax',
                    'Width',
                    'Height',
                    'Depth',
                    'Weight',
                    'Delivery_in_stock',
                    'Delivery_out_stock',
                    'Quantity',
                    'Minimal_quantity',
                    'Low_stock',
                    'Notification_low_stock',
                    'Visibility',
                    'Additional_shipping_cost',
                    'Unity',
                    'Unity_price',
                    'Short_description',
                    'Description',
                    'Tags',
                    'Meta_title',
                    'Meta_keywords',
                    'Meta_description',
                    'URL_rewritten',
                    'Text_when_in_stock',
                    'Text_when_backorder_allowed',
                    'Available_for_order',
                    'Product_available_date',
                    'Product_creation_date',
                    'Show_price',
                    'Image_URLs',
                    'Image_alt',
                    'Delete_existing_images',
                    'Feature',
                    'Web_only',
                    'Condition',
                    'Customizable',
                    'Uploadable_files',
                    'Text_fields',
                    'Out_of_stock',
                    'Virtual product',
                    'File URL',
                    'Allowed_downloads',
                    'Expiration_date',
                    'Number_of_days',
                    'ID_/_Name_of_shop',
                    'Advanced_stock_management',
                    'Depends_On_Stock',
                    'Warehouse',
                    'Accesories',
                    'id_p'
                ],
*/
            'combinacion'   =>  [
                'ID',
                'Product_Reference',
                'Attribute',
                'Value',
                'Supplier_reference',
                'Reference',
                'EAN13',
                'UPC',
                'Wholesale_price',
                'Impact_on_price',
                'Ecotax',
                'Quantity',
                'Minimal_quantity',
                'Impact_on_weight',
                'Default',
                'Combination_available_date',
                'Image_position',
                'Image_URL',
                'Image Alt',
//                'Delete_existing_images',
                'ID_/_Name_of_shop',
                'Advanced_Stock_Managment',
                'Depends_on_stock',
                'Warehouse'
            ],
            'precio-especifico' =>  [
                'id_producto',
                'referencia',
                'descuento'
            ],
            'producto-transportista'    => [
                'id_producto',
                'id_transportista'
            ],
            'marca' =>  [
                'ID',
                'Active',
                'Name',
                'Description',
                'Short description',
                'Meta title',
                'Meta keywords',
                'Meta description',
                'Image URL'
            ],
            'categoria' =>  [
                'ID',
                'Active',
                'Name',
                'Parent category',
                'Root category',
                'Description',
                'Meta title',
                'Meta keywords',
                'Meta description',
                'URL',
                'Image URL'
            ]
        ]
    ]

];