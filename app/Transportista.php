<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportista extends Model {

    protected $table= 'transportista';

    public function productos()
    {
        return $this->belongsToMany('App\Producto','producto_transportista','transportista_id','producto_id');
    }

}
