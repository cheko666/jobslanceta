<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Existencia extends Model {

    protected $table = 'existencia';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id_producto'
    ];

//    use SoftDeletes;

    public function codigo()
    {
        return $this->belongsTo('App\Codigo');
    }

}
