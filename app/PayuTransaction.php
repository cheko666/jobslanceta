<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PayuTransaction extends Model {

	protected $table = 'payu_transactions';

    public function scopeOffLinePayment()
    {

    }

    public function scopeApprovedTransaction($query)
    {
        return $query->where('codigo_respuesta','APPROVED');
    }

}
