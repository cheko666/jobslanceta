<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Producto extends Model
{

    use SoftDeletes;

    protected $perPage = 12;

    protected $table = 'productos';

    protected $fillable = [
        'nombre',
        'marca_id',
        'codigo',
        'serie',
        'url',
        'descripcion',
        'pais',
        'foto_nombre',
        'foto_tamano',
        'foto_nombre_original',
        'foto_fuente',
        'activo',
        'creando',
        'destacado'
    ];

    public function codigos()
    {
        return $this->hasMany('App\Codigo');
    }

    public function marca()
    {
        return $this->hasOne('App\Marca','id','marca_id');
    }

    public function transportistas()
    {
        return $this->belongsToMany('App\Transportista','producto_transportista','producto_id','transportista_id');
    }

    public function scopeExportToPS($query)
    {
        return $query->
        select(
            DB::raw("
        @pid := productos.id as id ,
        if(productos.activo = 1 and productos.deleted_at is null ,1,0) as active,
        productos.nombre as name ,
        (
            select group_concat(categoria.id order by categoria.nivel desc separator ';')
            from categoria
            join producto_categoria cp on cp.categoria_id = categoria.id and cp.producto_id = @pid
        )as categories ,
        c.precio as price ,
        if(c.iva = 'N',2, 1) as tax_rule_id ,
        '' as wholesale_price ,
        '' as on_sale ,
        '' as discount_amount ,
        '' as discoun_percent ,
        '' as discoun_from ,
        '' as discoun_to ,
        c.id as Reference ,
        c.modelo as supplier_reference ,
        left(c.id,3) as supplier ,
        ( select marcas.nombre from marcas where marcas.id = c.marca_id) as marca ,
        '' as ean ,
        '' as upc ,
        '' as ecotax ,
        c.ancho as width ,
        c.altura as height ,
        c.fondo as depth ,
        c.peso as weight ,
        ( IFNULL( (SELECT quantity FROM production_presta.ps0516_stock_available WHERE id_product = @pid AND id_product_attribute = 0 limit 1) ,0) ) as quantity ,
        '' as minimal_quantity ,
        'both' as visibility ,
        '' as aditional_shipping_cost ,
        c.unidad as unity ,
        '' as unity_price ,
        '' as short_description ,
        productos.descripcion as description ,
        ( select @tags := group_concat(tags.nombre order by nombre separator ';') from tags where tags.producto_id = productos.id) as tags ,
        productos.nombre as meta_title ,
        @tags as meta_keywords ,
        '' as meta_description ,
        productos.url as url_written ,
        'Producto Disponible' as text_when_stock ,
        '' as text_when_backorder ,
        if(cd.c_con_dim >= 1 and productos.estafeta = 1, 1 ,0) as available_for_order ,
        #1 as available_for_order ,
        '' as available_date ,
        date(productos.created_at) as creation_date ,
        1 as show_price ,
        if( ( select @img_nom := i.nombre from imagenes i where i.producto_id = productos.id and i.deleted_at is null ) is not null, concat('http://media.lancetahg.com.mx/p/', @img_nom ) , '' ) as image_url ,
        0 as delete_images,
        if( (select @caracteristicas := group_concat(pe.nombre,':',pe.valor,':',pe.posicion order by posicion separator ';') from productos_especificaciones pe where pe.producto_id = productos.id ) is not null, @caracteristicas,'' ) as features ,
        '' as available_web_only ,
        '' as 'condition' ,
        '' as uploadable_files ,
        '' as customizable ,
        '' as text_fields ,
        '' as out_of_stock ,
        '' as id_shop ,
        '' as advanced_stockManagement ,
        '' as depends_onStock ,
        '' as warehouse
                ")

        )->from('lanceta_bd_ec.productos')
        ->join(DB::raw('
                (
                    select * from lanceta_bd_ec.codigos where codigos.principal = 1 order by codigos.producto_id
                ) as c
                '), function($join) {
            $join->on('c.producto_id','=','productos.id');
        })
        ->join(DB::raw('(
                        select codigos.producto_id,
                        count(*) as cant_cod,
                        count(case when (codigos.peso > 0 and codigos.altura > 0 and codigos.ancho > 0 and codigos.fondo > 0 ) then 1 end) as c_con_dim,
                        count(case when (codigos.maximo > 0 ) then 1 end) as c_con_max,
                        sum(codigos.existencia) existencia
                        from lanceta_bd_ec.codigos
                        group by codigos.producto_id
                    ) as cd'), function($join){
            $join->on('cd.producto_id','=','productos.id');
        })
        ;
    }

    public function scopeExportToPS17($query)
    {
        return $query->
        select(
            DB::raw("
@pid := productos.id as id ,
if(productos.activo = 1 and productos.deleted_at is null ,1,0) as active,
productos.nombre as name ,
(
    select group_concat(categoria.id order by categoria.nivel desc separator ';')
    from categoria
    join producto_categoria cp on cp.categoria_id = categoria.id and cp.producto_id = @pid
)as categories ,
c.precio as price ,
if(c.iva = 'N',2, 1) as tax_rule_id ,
'' as wholesale_price ,
'' as on_sale ,
'' as discount_amount ,
'' as discoun_percent ,
'' as discoun_from ,
'' as discoun_to ,
c.id as Reference ,
c.modelo as supplier_reference ,
left(c.id,3) as supplier ,
( select marcas.nombre from marcas where marcas.id = c.marca_id) as marca ,
'' as ean ,
'' as upc ,
'' as ecotax ,
c.ancho as width ,
c.altura as height ,
c.fondo as depth ,
c.peso as weight ,
( IFNULL( (SELECT quantity FROM production_presta.ps0516_stock_available WHERE id_product = @pid AND id_product_attribute = 0 limit 1) ,0) ) as quantity ,
'' as minimal_quantity ,
'both' as visibility ,
'' as aditional_shipping_cost ,
c.unidad as unity ,
'' as unity_price ,
'' as short_description ,
productos.descripcion as description ,
( select @tags := group_concat(tags.nombre order by nombre separator ';') from tags where tags.producto_id = productos.id) as tags ,
productos.nombre as meta_title ,
@tags as meta_keywords ,
'' as meta_description ,
productos.url as url_written ,
'Producto Disponible' as text_when_stock ,
'' as text_when_backorder ,
if(cd.c_con_dim >= 1 and productos.estafeta = 1, 1 ,0) as available_for_order ,
#1 as available_for_order ,
'' as available_date ,
date(productos.created_at) as creation_date ,
1 as show_price ,
if( ( select @img_nom := i.nombre from imagenes i where i.producto_id = productos.id and i.deleted_at is null ) is not null, concat('http://media.lancetahg.com.mx/p/', @img_nom ) , '' ) as image_url ,
c.descripcion AS image_alt ,
0 as delete_images,
if( (select @caracteristicas := group_concat(pe.nombre,':',pe.valor,':',pe.posicion order by posicion separator ';') from productos_especificaciones pe where pe.producto_id = productos.id ) is not null, @caracteristicas,'' ) as features ,
'' as available_web_only ,
'' as 'condition' ,
'' as uploadable_files ,
'' as customizable ,
'' as text_fields ,
'' as out_of_stock ,
'' as id_shop ,
'' as advanced_stockManagement ,
'' as depends_onStock ,
'' as warehouse ,
'' AS accesories
            ")
            )->from('lanceta_bd_ec.productos')
            ->join(DB::raw('
                (
                    select * from lanceta_bd_ec.codigos where codigos.principal = 1 order by codigos.producto_id
                ) as c
                '), function($join) {
                $join->on('c.producto_id','=','productos.id');
            })

            ->join(DB::raw('(
                        select codigos.producto_id,
                        count(*) as cant_cod,
                        count(case when (codigos.peso > 0 and codigos.altura > 0 and codigos.ancho > 0 and codigos.fondo > 0 ) then 1 end) as c_con_dim,
                        count(case when (codigos.maximo > 0 ) then 1 end) as c_con_max,
                        sum(codigos.existencia) existencia
                        from lanceta_bd_ec.codigos
                        group by codigos.producto_id
                    ) as cd'), function($join){
                $join->on('cd.producto_id','=','productos.id');
            })

            ;
    }


    public function scopeExportToPS17Simple($query)
    {
        return $query->
        select(
            DB::raw("
CAST(c.id AS UNSIGNED) as id ,
if(c.baja = 'N' and c.deleted_at is null ,1,0) as active,
#c.baja,
#c.por_agotar,
REPLACE(REPLACE(REPLACE(c.descripcion,'¤','Ñ'),'#','NO.'),'¥','Ñ') as name ,
(
    select group_concat(categoria.id order by categoria.nivel desc separator ';')
    from lanceta_bd_ec.categoria
    join lanceta_bd_ec.producto_categoria cp on cp.categoria_id = categoria.id and cp.producto_id = @pid
)as categories ,
c.precio as price ,
if(c.iva = 'N',2, 1) as tax_rule_id ,
'' as wholesale_price ,
'' as on_sale ,
'' as discount_amount ,
'' as discoun_percent ,
'' as discoun_from ,
'' as discoun_to ,
LPAD(c.id,6,'0') as Reference ,
c.modelo as supplier_reference ,
left(c.id,3) as supplier ,
( select marcas.nombre from lanceta_bd_ec.marcas where marcas.id = c.marca_id) as marca ,
'' as ean ,
'' as upc ,
'' as npm ,
'' as ecotax ,
c.ancho as width ,
c.altura as height ,
c.fondo as depth ,
c.peso as weight ,
'' as delivery_in_stock ,
'' as delivery_out_stock ,
0 as quantity ,
'' as minimal_quantity ,
'1' as low_stock ,
'0' as notification_low_stock ,
'both' as visibility ,
'' as aditional_shipping_cost ,
c.unidad as unity ,
'' as unity_price ,
'' as short_description ,
productos.descripcion as description ,
( select @tags := group_concat(tags.nombre order by nombre separator ';') from lanceta_bd_ec.tags where tags.producto_id = productos.id) as tags ,
productos.nombre as meta_title ,
@tags as meta_keywords ,
'' as meta_description ,
productos.url as url_written ,
'Producto Disponible' as text_when_stock ,
'' as text_when_backorder ,
if(c.c_con_dim >= 1 and productos.estafeta = 1, 1 ,0) as available_for_order ,
'' as available_date ,
date(productos.created_at) as creation_date ,
1 as show_price ,
IF(
	(SELECT @img_nom1 := pai.id_image
	FROM production_presta.ps0516_product_attribute_image pai
	JOIN production_presta.ps0516_product_attribute pa ON pa.id_product_attribute = pai.id_product_attribute
	WHERE pa.reference = c.codigo AND pa.id_product = productos.id LIMIT 1) IS NOT NULL
, @img_nom1
, if( ( select @img_nom2 := i.nombre from lanceta_bd_ec.imagenes i where i.producto_id = productos.id and i.deleted_at is null ) is not null, @img_nom2  , '' )
) as image_url ,
#if( ( select @img_nom := i.nombre from lanceta_bd_ec.imagenes i where i.producto_id = productos.id and i.deleted_at is null ) is not null, concat('http://media.lancetahg.com.mx/p/', @img_nom ) , '' ) as image_url ,
productos.nombre as image_alt ,
0 as delete_images ,
if( (select @caracteristicas := group_concat(pe.nombre,':',pe.valor,':',pe.posicion order by posicion separator ';') from lanceta_bd_ec.productos_especificaciones pe where pe.producto_id = productos.id ) is not null, @caracteristicas,'' ) as features ,
'' as available_web_only ,
'' as 'condition' ,
'' as 'customizable' ,
'' as uploadable_files ,
'' as customizable ,
'' as text_fields ,
'' as out_of_stock ,
'' AS 'Virtual_product',
'' AS 'File URL',
'' AS 'Allowed_downloads',
'' AS 'Expiration_date',
'' AS 'Number_of_days',
'' as id_shop ,
'' as advanced_stockManagement ,
'' as depends_onStock ,
'' as warehouse,
'' AS accesories ,
@pid := c.producto_id AS pid
            ")

            )->from(DB::raw('(
                SELECT *
                , LPAD(codigos.id,6,\'0\') AS codigo
                , case when (codigos.peso > 0 and codigos.altura > 0 and codigos.ancho > 0 and codigos.fondo > 0 ) then 1 end AS c_con_dim
                FROM lanceta_bd_ec.codigos
                WHERE codigos.id > 1002
            ) AS c'))
            ->join(DB::raw('lanceta_bd_ec.productos'), function($join) {
                $join->on('c.producto_id','=','productos.id');
            })
            /*
            ->join(DB::raw('(
                        select codigos.producto_id,
                        count(*) as cant_cod,
                        count(case when (codigos.peso > 0 and codigos.altura > 0 and codigos.ancho > 0 and codigos.fondo > 0 ) then 1 end) as c_con_dim,
                        count(case when (codigos.maximo > 0 ) then 1 end) as c_con_max,
                        sum(codigos.existencia) existencia
                        from lanceta_bd_ec.codigos
                        group by codigos.producto_id
                    ) as cd'), function($join){
                $join->on('cd.producto_id','=','productos.id');
            })
            */
            ;
    }

    public function scopeExportToPSProducto($query,$movimiento = null, $date_to_export = null,$version = null,$con_atributos = true)
    {
        if($version && $version == '17')
        {
            switch($movimiento)
            {
                case 'alta':
                    return $query->exportToPS17()->where('productos.created_at','>', $date_to_export->toDateTimeString());
                    break;
                case 'modificacion':
                    return $query->exportToPS17()->where('productos.updated_at','>', $date_to_export->toDateTimeString());
                    break;
                case 'baja':
                    return $query->onlyTrashed17()->select('id')->where('productos.deleted_at','>', $date_to_export->toDateTimeString());
                    break;
                case 'todo':
                    return $con_atributos ? $query->exportToPS17()->where('productos.updated_at','>', $date_to_export->toDateTimeString()) : $query->exportToPS17Simple()->where('productos.updated_at','>', $date_to_export->toDateTimeString());
                    break;
                default :
                    return $query->exportToPS17();
            }
        }
        else
        {
            switch($movimiento)
            {
                case 'alta':
                    return $query->exportToPS()->where('productos.created_at','>', $date_to_export->toDateTimeString());
                    break;
                case 'modificacion':
                    return $query->exportToPS()->where('productos.updated_at','>', $date_to_export->toDateTimeString());
                    break;
                case 'baja':
                    return $query->onlyTrashed()->select('id')->where('productos.deleted_at','>', $date_to_export->toDateTimeString());
                    break;
                case 'todo':
                    return $query->exportToPS();
                    break;
                default :
                    return $query->exportToPS();
            }
        }


    }

    public function scopeExportToPSProductoTransportista($query)
    {
        return $query
            ->select(
                DB::raw('
                pt.producto_id as id, pt.transportista_id
                ')
            )
            ->from('productos')
            ->join('producto_transportista as pt', function ($join){
                $join->on('pt.producto_id','=','productos.id');
            })
            ->orderBy('id');
    }

}
