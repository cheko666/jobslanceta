<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{

    protected $table = 'query';

    protected $fillable = [
        'statement',
        'descripcion',
        'entidad',
        'archivo_nombre',
    ];


    public function export()
    {
        return $this->belongsTo('\App\Export');
    }

}
