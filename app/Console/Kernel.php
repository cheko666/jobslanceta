<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
//		'App\Console\Commands\Inspire',
		'App\Console\Commands\EditarMarca',
		'App\Console\Commands\ExportCsvToPSMarcaModificacion',
		'App\Console\Commands\ExportCsvToPSProductoModificacion',
		'App\Console\Commands\ExportCsvToPSPrecioEspecifico',
		'App\Console\Commands\ExportCsvToPSProductoTransportista',
//		'App\Console\Commands\ExportCsvToPSProductoBaja',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
/*
		$schedule->command('editar:marcasNuevas')
			->dailyAt('1:15');

		$schedule->command('exportToPS:marcas_modificacion')
       ->dailyAt('1:17');

		$schedule->command('exportToPS:preciosEspecificos_todo')
       ->dailyAt('1:20');

		$schedule->command('exportToPS:productosTransportistas_todo')
       ->dailyAt('1:22');

		$schedule->command('exportToPS:productos_modificacion')
       ->dailyAt('1:25');
*/
	}

}
