<?php namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use Illuminate\Console\Command;


class ExportCsvToPSProductoBaja extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'exportToPS:productos_baja';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Baja de productos del día anterior';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExportController $exportController)
	{
		parent::__construct();

		$this->exportController = $exportController;

	}

	public function handle()
	{
		$this->exportController->exportCsv('producto','baja');
	}


}
