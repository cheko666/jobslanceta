<?php namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExportCsvToPSProductoModificacion extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'exportToPS:productos_modificacion';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExportController $exportController)
	{
		parent::__construct();

		$this->exportController = $exportController;
	}

	public function handle()
	{

		$this->exportController->exportCsv('producto','modificacion');


	}


}
