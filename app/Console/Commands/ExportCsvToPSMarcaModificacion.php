<?php namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use Illuminate\Console\Command;

class ExportCsvToPSMarcaModificacion extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'exportToPS:marcas_modificacion';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Modificación de marcas del día anterior';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExportController $exportController)
	{
		parent::__construct();

		$this->exportController = $exportController;
	}

	public function handle()
	{
		$this->exportController->exportCsv('marca','modificacion');
	}

}
