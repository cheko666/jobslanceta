<?php namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EditarMarca extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'editar:marcasNuevas';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Arreglar el nombre y url de la marca nueva que genera Magic';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExportController $exportController)
	{
		parent::__construct();

		$this->exportController = $exportController;
	}

	public function handle()
	{
		$this->exportController->updateMarcasNuevas();
	}

}
