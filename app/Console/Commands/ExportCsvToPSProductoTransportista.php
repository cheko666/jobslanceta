<?php namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use Illuminate\Console\Command;

class ExportCsvToPSProductoTransportista extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'exportToPS:productosTransportistas_todo';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Todos los registros de producto-transportista';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExportController $exportController)
	{
		parent::__construct();

		$this->exportController = $exportController;
	}

	public function handle()
	{
		$this->exportController->exportCsv('producto-transportista');
	}

}
