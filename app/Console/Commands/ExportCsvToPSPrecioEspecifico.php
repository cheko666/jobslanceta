<?php namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use Illuminate\Console\Command;

class ExportCsvToPSPrecioEspecifico extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'exportToPS:preciosEspecificos_todo';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Todos los registros de descuentos (precios específicos)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ExportController $exportController)
	{
		parent::__construct();

		$this->exportController = $exportController;
	}

	public function handle()
	{
		$this->exportController->exportCsv('precio-especifico');
	}

}
