<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Marca extends Model {

    use SoftDeletes;

    protected $table = 'marcas';

    protected $fillable = [
        'nombre',
        'url'
    ];

    public function productos()
    {
        return $this->hasMany('App\Producto','marca_id')->where('activo',1);
    }

    public function codigo()
    {
        return $this->belongsTo('App\Marca');
    }

    public function getNombreTmpAttribute()
    {

        return ucwords(strtolower($this->nombre_upper));
    }

    public function getUrlTmpAttribute()
    {

        return str_slug($this->nombre_upper);
    }

    public function scopeExportToPS($query)
    {
        return $query->select(DB::raw("
                id as id,
                activo as Active,
                nombre as Name,
                '' as Description,
                '' as Short_description,
                '' as Meta_title,
                '' as Meta_keywords,
                '' as Meta_description,
                '' as Image_URL,
                '' as id_shop
            "))
            ->from('marcas')
            ->where('id','>',0)
            ->where('nombre_upper','<>','')
            ->orderBy('ID')
            ;
    }

    public function scopeExportToPSMarca($query,$movimiento, $date_to_export)
    {

        switch($movimiento)
        {
            case 'alta':
                return $query->exportToPS()->where('created_at','>', $date_to_export->toDateTimeString());
                break;
            case 'modificacion':
                return $query->exportToPS()->where('updated_at','>', $date_to_export->toDateTimeString());
                break;
            case 'baja':
                return $query->onlyTrashed()->select('id')->where('deleted_at','>', $date_to_export->toDateTimeString());
                break;
            case 'todo':
                return $query->exportToPS();
                break;
        }

    }
}
