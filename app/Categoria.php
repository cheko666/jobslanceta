<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria extends Model
{
    protected $table = 'categoria';


    public function scopeExportToPSCategoria($query)
    {
        return $query
            ->select(
                DB::raw('
                    c1.id,
                    1 active,
                    c1.nombre name,
                    if(c1.nivel = 1 or c1.id = 8 or c1.id = 9 ,\'Inicio\',(select c2.nombre from categoria c2 where c2.id = c1.padre)) as parent,
                    0 root,
                    c1.descripcion description,
                    c1.nombre meta_title,
                    \'\' meta_keyword,
                    \'\' as meta_description,
                    \'\' url,
                    \'\' as image_url
                ')
            )
            ->from('categoria as c1')
            ->where('id','>',2)
            ->orderBy('c1.id')
            ->orderBy('c1.nombre');
    }


}
