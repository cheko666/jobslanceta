<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export extends Model
{

    protected $table = 'export';

    protected $fillable = [
        'id',
        'user_id',
        'entidad',
        'bd',
        'tipo',
        'tiempo',
        'cron',
        'query_archivo',
        'archivo',
        'ids_export',
        'headers'
    ];

    public function querys()
    {
        return $this->hasOne('\App\Query','id');
    }

    public function scopeByEntidad($query,$entidad)
    {
        return $query->where('entidad','like',$entidad);
    }

    public function scopeByTipo($query,$tipo)
    {
        return $query->where('tipo','like',$tipo);
    }


}
