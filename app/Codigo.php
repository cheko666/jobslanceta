<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Codigo extends Model
{

    protected $table = 'codigos';

    protected $perPage = 30;

    protected $fillable = [
        'producto_id',
        'principal',
        'registro_sanitario',
        'peso',
        'altura',
        'ancho',
        'fondo'
    ];

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }

    public function marca()
    {
        return $this->hasOne('App\Marca','id','marca_id');
    }

    public function existencia()
    {
        return $this->hasOne('App\Existencia','id','id');
    }

    function getReferenceAttribute() {
        return str_pad($this->reference,6,'0',STR_PAD_LEFT);
    }

    public function scopeImportToAppExistencia()
    {

    }

    public function scopeExportToPSCombinacion($query,$ids_productos)
    {
        return $query->
        select(
            DB::raw("
                @idp := c.producto_id AS id,
                c.id AS 'Product_Reference',
                ( GROUP_CONCAT(a.atributo,':',a.tipo,':',a.posicion ORDER BY posicion SEPARATOR ';') ) as Attribute,
                ( GROUP_CONCAT(a.valor,':',a.posicion ORDER BY posicion SEPARATOR ';') ) as 'Value',
                c.modelo AS 'Supplier reference',
                c.id AS 'Reference',
                '' AS EAN13,
                '' AS UPC,
                '' AS 'Wholesale price',
                #Diferencia respecto al codigo de menor precio del grupo de productos
                c.precio - cd.precio AS 'Impact on price',
                '' AS Ecotax,
                #Sólo existencia de eCommerce
                ps.quantity AS Quantity,
                '' AS 'Minimal quantity',
                #Diferencia respecto al codigo de menor peso del grupo de productos
                c.peso - cd.peso AS 'ImpactOnWeight',
                c.principal AS 'Default',
                #CAST(NOW() AS DATE) AS 'Combination available date',
                '' AS 'Combination available date',
                '' AS 'Image position',
                '' AS 'Image URL',
                GROUP_CONCAT(a.atributo,' ',a.valor ORDER BY posicion SEPARATOR '-') AS 'Image Alt',
                #0 AS 'Delete existing images',
                '' AS 'ID / Name of shop',
                '' AS 'Advanced Stock Managment',
                '' AS 'Depends on stock',
                '' AS 'Warehouse'
                #, ps.*
                ")

        )->from('lanceta_bd_ec.codigos as c')
        ->join('lanceta_bd_ec.productos as p', function($join) {
            $join->on('c.producto_id','=','p.id');
        })
        ->leftJoin(DB::raw('(select * from lanceta_bd_ec.codigos where codigos.principal = 1) as cd '), function($join){
            $join->on('cd.producto_id','=','p.id');
        })
        ->join(DB::raw('(
                    select *
                    FROM lanceta_bd_ec.atributos
                    WHERE atributos.atributo IS NOT NULL AND atributos.valor IS NOT NULL
                    ) as a'), function($join) {
            $join->on('a.codigo_id','=','c.id');
        })
        ->leftJoin(DB::raw("(
          select
          ifnull(pa.reference,p2.reference) as ps_reference,
          p2.id_product as id_product,
          LPAD(p2.id_manufacturer,3,0) ps_marca_id,
          round(p2.price,2) as price,
          ifnull(pa.id_product_attribute,0) id_product_attribute,
          ifnull(pa.supplier_reference,p2.supplier_reference) as ps_modelo,
          round(ifnull(pa.price,0),2) pa_price,
          round(ifnull(pas.price,0),2) pas_price,
          round( ifnull(pa.weight,0.000) + p2.weight, 3) as ps_peso,
          round( p2.width, 3) as ps_ancho,
          round( p2.height, 3) as ps_altura,
          round( p2.depth, 3) as ps_fondo,
          round(ifnull(sp.descuento,0)) as ps_descuento,
          #ifnull(sp.id_specific_price,0) id_specific_price,
          #round(ifnull(sp.reduction,0),2) reduction,
          round( ifnull(pa.price,0) + p2.price,2 ) as precio_base_ps,
          round( ifnull(pas.price,0) + p2.price,2 ) as ps_precio,
          round( (ifnull(pa.price,0) + p2.price) * ( if(p2.id_tax_rules_group = 1,1.16,1) ) ,2 ) as ps_precio_neto,
          if(p2.id_tax_rules_group = 1,16,0) as ps_iva
          , sa.quantity
          from production_presta.ps0516_product p2
          left join (
            select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference
            from production_presta.ps0516_product_attribute
          ) pa on pa.id_product = p2.id_product
          left join production_presta.ps0516_product_attribute_shop pas on pas.id_product_attribute = pa.id_product_attribute
          left join (
            SELECT
            *,
            ps0516_specific_price.reduction*100 as descuento
            FROM production_presta.ps0516_specific_price
            where reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0
          ) as sp on sp.id_product = p2.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
          left join production_presta.ps0516_stock_available sa ON sa.id_product = p2.id_product AND sa.id_product_attribute = pa.id_product_attribute
          where p2.active = 1
        ) as ps
        "), 'c.id', '=', 'ps.ps_reference')

        ->whereIn('c.producto_id',$ids_productos)
        ->whereNotNull('c.producto_id')
        ->where('c.baja','=','N')
        ->groupBY('c.id')
        ->orderBy('id');
    }

    public function scopeExportToPSPrecioEspecifico($query,$ps_version)
    {
        if($ps_version && $ps_version == '17')
        {
            return $query
                ->select(
                    DB::raw('
                        CAST(c.id AS UNSIGNED) as id_product, c.descuento
                    ')
                )
                ->from('codigos as c')
                ->where('c.descuento','>',0)
                ->orderBy('c.id');
        }
        else
        {
            return $query
                ->select(
                    DB::raw('
                        p.id as id_product, c.id as Reference, c.descuento
                    ')
                )
                ->from('codigos as c')
                ->join('productos as p', function($join){
                   $join->on('c.producto_id','=','p.id');
                })
                ->where('c.descuento','>',0)
                ->orderBy('p.id');
        }
    }

    public function scopeExportToPSPrecioEspecificoMagic($query)
    {
        return $query
            ->select(
                DB::raw('
                    p.id as id_product, concat(LPAD(c.Serie,3,0),LPAD(c.Producto,3,0)) as Reference, c.Descuento_Producto
                ')
            )
            ->from('magic.Productos as c')
            ->join('lanceta_bd_ec.productos as p', function($join){
                $join->on('c.Grupo_(ID)','=','p.id');
            })
            ->where('c.Descuento_Producto','>',0)
            ->orderBy('p.id');
    }
/*    public function getImpactOnWeightAttribute()
    {
        return (string)$this->ImpactOnWeight;
    }*/
}
