<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/***************    Admin routes  **********************************/
Route::group([ 'middleware' => 'auth'], function() {

	Route::get('/', 'HomeController@index');

	// Exportaciones
	Route::get('export/csv/{entidad}/{tipo?}/{cantidad?}-{periodo?}/{ids?}',['as'=>'export.tocsv','uses'=>'ExportController@exportCsv']);
	Route::get('export',['as'=>'export.index','uses'=>'ExportController@index']);
	Route::get('export/create',['as'=>'export.create','uses'=>'ExportController@create']);
	Route::post('export',['as'=>'export.store','uses'=>'ExportController@store']);
	Route::get('marca/updateMarcasNuevas',['as'=>'updateMarcasNuevas','uses'=>'ExportController@updateMarcasNuevas']);
/*	Route::get('export/to-csv/producto/{tipo?}/{cantidad?}-{periodo?}',['as'=>'export.tocsv.productos','uses'=>'ExportController@exportToCsvProductos']);
	Route::get('export/to-csv/precio-especifico',['as'=>'export.tocsv.precios-especificos','uses'=>'ExportController@exportToCsvPreciosEspecificos']);
	Route::get('export/to-csv/producto-transportista',['as'=>'export.tocsv.producto-transportista','uses'=>'ExportController@exportToCsvProductoTransportista']);
	Route::get('export/to-csv/producto-baja',['as'=>'export.tocsv.productos-baja','uses'=>'ExportController@exportToCsvProductosBaja']);
*/
	Route::get('email/exportcsv',function(){
		$title = 'Notificación de exportación de datos';

		return view('emails.exportcsv',compact('title'));
	});
	Route::get('forms',function(){

		return view('form');
	});
});