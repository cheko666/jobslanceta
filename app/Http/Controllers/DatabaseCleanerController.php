<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DatabaseCleanerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function cleanPSDatabase()
    {
        $query = DB::affectingStatement("
            # Delete all logs
            TRUNCATE ps_log;


            # Delete old connection data (only used for stats)
            # change 2016-02-01 00:00:00 according to you needs
            DELETE c, cs
            FROM production_presta.ps0516_connections c
            LEFT JOIN production_presta.ps0516_connections_source cs ON (c.id_connections = cs.id_connections)
            LEFT JOIN production_presta.ps0516_guest g ON c.id_guest = g.id_guest
            WHERE g.id_customer = 0
            and c.date_add < '2019-01-01 00:00:00'
            ;

            OPTIMIZE TABLE ps_connections, ps_connections_source;

            SELECT *
            FROM production_presta.ps0516_connections
            #where id_guest in (SELECT ps0516_guest.id_guest FROM production_presta.ps0516_guest where ps0516_guest.id_customer)
            ;


            # Delete all guest without entry in ps_customer table
            DELETE g
            FROM production_presta.ps0516_guest g
            LEFT JOIN production_presta.ps0516_customer c ON (g.id_customer = c.id_customer)
            WHERE c.id_customer IS NULL
            and g.id_customer = 0
            ;

            OPTIMIZE TABLE production_presta.ps0516_guest;

# 404 erors
            DELETE FROM production_presta.ps0516_pagenotfound WHERE `date_add` < 'YYYY-MM-DD'
            ;

DELETE FROM `'._DB_PREFIX_.'cart`
		WHERE id_cart NOT IN (SELECT id_cart FROM `'._DB_PREFIX_.'orders`)
		AND date_add < ".pSQL(date('Y-m-d', strtotime('-1 month')))."';

# Sent mails
DELETE FROM `ps_mail` WHERE `date_add` < 'YYYY-MM-DD';

            # Delete tables
            # Scenes are deprecated in 1.6 (used only if upgrading with feature active)
            DROP TABLE `ps_scene`;
            DROP TABLE `ps_scene_category`;
            DROP TABLE `ps_scene_lang`;
            TRUNCATE `ps_scene_products`;
            DROP TABLE `ps_scene_shop`;
            UPDATE `ps_configuration` SET value='0', date_upd=NOW() WHERE `name` = 'PS_SCENE_FEATURE_ACTIVE';



        ");
    }
}
