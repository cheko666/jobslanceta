<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Codigo;
use App\Export;
use App\Marca;
use App\Producto;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;
use SplTempFileObject;

class ExportController extends Controller
{
    protected $_ps_version;
    protected $_con_atributos = true;

    public function __construct(Request $request)
    {
        $this->_ps_version = $request->has('psv') ? $request->get('psv') : '';
        if($request->has('atrib') && $request->get('atrib') == '0')
            $this->_con_atributos = false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($entidad = null,$tipo = null)
    {
        $title = 'Exportaciones realizadas';


        if($entidad && $entidad != null)
        {
            if($tipo && $tipo != null)
            {
                $exports = Export::byEntidad($entidad)->byTipo($tipo)->orderBy('created_at','desc')->get();

            } else {

                $exports = Export::byEntidad($entidad)->orderBy('created_at','desc')->get();

            }

        } else {

            $exports = Export::orderBy('created_at','desc')->get();

        }

        return view('exports.index',compact('title','exports'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = 'Registrar nueva exportación';

        return view('exports.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        dd($request);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadCsvExport(Request $request)
    {


    }

    public function createdToCsv()
    {

    }

    /**
     * @param $entidad
     * @param string $movimiento
     * @param int $cantidad
     * @param string $periodo
     * @param null $ids_productos
     */
    public function exportCsv($entidad,$movimiento = 'todo',$cantidad=1,$periodo='d',$ids_productos = null)
    {
        $title = 'Exportación de datos a PS (csv)';

        $headers = Request::capture()->header();
        $uri_segments = Request::capture()->segments();

        $user = array_keys($uri_segments, 'export') || array_keys($uri_segments, 'csv') ? Auth::user() : (object)['name'=>'Cron'];

        // Fecha y Hora fijada al momento que
        $date = Carbon::now();
        $tiempo = $cantidad . '-' . $periodo;
        $date_to_export = $this->getDateToExport($date,$cantidad,$periodo);

        $date_csv = Carbon::now()->format('Ymd_His');

        /*******************************************************
         * Obtener datos
         *******************************************************/
        if($ids_productos)
        {
            $collection = $this->getCollectionFromId(explode(',',$ids_productos),$date_to_export);

        } else {

            $collection = $this->getCollection($entidad,$movimiento,$date_to_export,$ids_productos,$this->_ps_version);
        }
        switch($entidad)
        {
            case 'producto-transportista':
                $ids = $collection->unique('id')->lists('id');
                break;
            case 'precio-especifico':
                $ids = null;
                break;
            default:
                $ids = $collection->lists('id');
                break;
        }

        $movimiento = ($movimiento == 'alta') ? 'modificacion' : $movimiento;
        /*******************************************************
         * Generar CSV's
         *******************************************************/
        $csv_nombre = $this->csvName($entidad,$movimiento,$date_csv);
        $csv_respaldo_nombre = 'export_'. $entidad . '_'. $movimiento . '_' . $date_csv . '.csv';
        $csv = $this->makeCsv($collection,$entidad,$movimiento,$date_csv);
//        dd($this->getExportPath() . $csv_nombre);

        if( ($entidad == 'marca' || $entidad == 'producto' || $entidad == 'combinacion') && $movimiento == 'modificacion') {

            if ( ! File::copy($this->getExportPath() . '/' . $entidad . '/'. $movimiento .'/' . $csv_nombre, $this->getExportPath() . '/' . $entidad . '/'. $movimiento .'/respaldo/' . $csv_respaldo_nombre ))
            {
                die("Couldn't copy file");
            }

        }


        /*******************************************************
         * Registrar en BD
         *******************************************************/
        $bd_record = $this->recordExport($entidad,$movimiento,$csv_nombre,$ids,$user,$tiempo);


        /*******************************************************
         * Generar CSV de Combinaciones, si entidad es producto
         *******************************************************/
        if($entidad == 'producto' && $bd_record && $movimiento != 'baja')
        {
            if($this->_con_atributos)
            {
                $collection_comb = $this->getCollection('combinacion',$movimiento,$date_to_export,$ids);
                $ids = $collection_comb->lists('Product_Reference');
                $csv_nombre_comb = $this->csvName('combinacion',$movimiento,$date_csv);
                $csv_respaldo_combinacion_nombre = 'export_combinacion_'. $movimiento . '_' . $date_csv . '.csv';
                $csv = $this->makeCsv($collection_comb,'combinacion',$movimiento,$date_csv);
                $bd_record = $this->recordExport('combinacion',$movimiento,$csv_nombre_comb,$ids,$user,$tiempo);
            }

            if( ($entidad == 'marca' || $entidad == 'producto' || $entidad == 'combinacion') && $movimiento == 'modificacion') {

                if ( ! File::copy($this->getExportPath() . '/combinacion/'. $movimiento .'/' . $csv_nombre_comb, $this->getExportPath() . '/combinacion/'. $movimiento .'/respaldo/' . $csv_respaldo_combinacion_nombre ))
                {
                    die("Couldn't copy file");
                }

            }

        }

        /*******************************************************
         * Enviar correo de notificación
         *******************************************************/
        $data_mail = [
            'title' =>  $title,
            'usuario' => $user->name,
            'user_id'=> $user->name=='Cron' ? '' : $user->id,
            'headers' =>    $headers,
            'attach' => $entidad == 'producto' && $movimiento != 'baja' && $this->_ps_version != '17' ? $csv_nombre . ' y ' . $csv_nombre_comb : $csv_nombre ,
            'entidad' => $entidad == 'producto' ? $entidad . ' y ' . 'combinacion' : $entidad,
            'movimiento' => $movimiento,
            'tiempo'    =>  $movimiento == 'todo' ? 'N/A' : $this->formatPeriodo($cantidad,$periodo) . ' atrás',
            'date_export' => $date_to_export->format('d-m-Y H:i:s'),
            'num_registros' => $entidad == 'producto' && $movimiento != 'baja' && $this->_ps_version != '17' ? 'Producto -> ' . $this->getFormatCountRecords(count($collection)) . ' / Combinación -> ' . $this->getFormatCountRecords(count($collection_comb)) : $this->getFormatCountRecords(count($collection))
        ];
        $data = $data_mail;

        Mail::send('emails.exportcsv', $data , function($message) use ($data_mail)
        {
            $message->from(config('psexport.email.from'));
            $message->to(config('psexport.email.to'))->subject('Exportación de ' . $data_mail['movimiento'] . ' en ' . $data_mail['entidad']);

        });



    }

    public function deletedToCsv()
    {

    }


    /**
     * Ubicacion del directorio de exportaciones.
     *
     * @return string
     */
    protected function getExportPath()
    {

        return Storage::disk('samba')->getDriver()->getAdapter()->getPathPrefix().'sincronizacion/magic/';
    }

    protected function getQueryPath()
    {
        return base_path('database/querys');
    }

    public function getDateToExport($date,$cantidad='1',$periodo='d')
    {

        if($periodo && $cantidad != '')
        {

            switch($periodo)
            {
                case 'm':
                    $date_pivot = $cantidad === '0' || $cantidad === '1' ? $date->startOfMonth() : $date->subMonth($cantidad)->startOfDay();
                    break;
                case 'd':
                    $date_pivot = $cantidad === '0' || $cantidad === '1' ? $date->startOfDay() : $date->subDays($cantidad)->startOfDay();
                    break;
                case 'h':
                    $date_pivot = $date->subHours($cantidad);
                    break;
                case 'i':
                    $date_pivot = $date->subMinutes($cantidad);
                    break;
                default:
                    $date_pivot = $date->yesterday()->startOfDay();
            }

        } else {

            $date_pivot = $date->yesterday()->startOfDay();
        }

        return $date_pivot;
    }


    private function formatPeriodo($cantidad,$periodo)
    {

        switch($periodo) {
            case 'd':
                return ($cantidad > 1) ? $cantidad . ' días' : $cantidad . ' día';
                break;
            case 'm':
                return ($cantidad > 1) ? $cantidad . ' meses' : $cantidad . ' mes';
                break;
            case 'Y':
                return ($cantidad > 1) ? $cantidad . ' años' : $cantidad . ' año';
                break;
            case 'h':
                return ($cantidad > 1) ? $cantidad . ' horas' : $cantidad . ' hora';
                break;
            case 'i':
                return ($cantidad > 1) ? $cantidad . ' minutos' : $cantidad . ' minuto';
                break;
        }
    }


    public function getCollection($entidad,$movimiento = null,$date_to_export = null,$ids_productos = null)
    {

        switch($entidad)
        {
            case 'producto':
                return $collect = Producto::exportToPSProducto($movimiento, $date_to_export,$this->_ps_version,$this->_con_atributos)->withTrashed()->orderBy('id')->get();
                break;
            case 'combinacion':
                return $collect = Codigo::exportToPSCombinacion($ids_productos)->get();
                break;
            case 'precio-especifico':
                return $collect = Codigo::exportToPSPrecioEspecifico($this->_ps_version)->get();
                break;
            case 'categoria':
                return $collect = Categoria::exportToPSCategoria()->get();
                break;
            case 'producto-transportista':
                return $collect = Producto::exportToPSProductoTransportista()->get();
                break;
            case 'marca':
                return $collect = Marca::exportToPSMarca($movimiento, $date_to_export)->get();
                break;
        }
    }

    public function getCollectionFromId($ids_productos,$date_to_export)
    {
        return $collect = Producto::exportToPSProducto('todo',$date_to_export,$this->_ps_version,$this->_con_atributos)->withTrashed()->whereIn('productos.id',$ids_productos)->orderBy('id')->get();

    }

    public function compararIdsExportados($date_export)
    {
        //        $ids_productos_modificados = array(implode(',', $ids_product));

        //Id's Exportados
        $productos_exportados = Export::where('created_at', '>', $date_export->toDateTimeString())
            ->where('entidad', 'producto')
            ->where('tipo', 'modificacion')
            ->limit(1)->get();

        $ids_productos_exportados = $productos_exportados->lists('ids_export');


//        $comparacion_ids = array_diff($ids_productos_modificados,$ids_productos_exportados);

        return false;
    }

    public function getHeadersCsv($entidad,$movimiento)
    {
        if($movimiento=='todo' || $movimiento=='modificacion' || $movimiento=='alta')
        {
            if($this->_ps_version == '17')
            {
                switch($entidad)
                {
                    case 'marca':
                        return config('psexport.cabeceras.v17.marca');
                        break;
                    case 'producto':
                        return config('psexport.cabeceras.v17.producto');
                        break;
                    case 'combinacion':
                        return config('psexport.cabeceras.v17.combinacion');
                        break;
                    case 'precio-especifico':
                        return config('psexport.cabeceras.v17.precio-especifico');
                        break;
                    case 'producto-transportista':
                        return config('psexport.cabeceras.v17.producto-transportista');
                        break;
                    case 'categoria':
                        return config('psexport.cabeceras.v17.categoria');
                        break;
                }
            }
            else
            {
                switch($entidad)
                {
                    case 'marca':
                        return config('psexport.cabeceras.v16.marca');
                        break;
                    case 'producto':
                        return config('psexport.cabeceras.v16.producto');
                        break;
                    case 'combinacion':
                        return config('psexport.cabeceras.v16.combinacion');
                        break;
                    case 'precio-especifico':
                        return config('psexport.cabeceras.v16.precio-especifico');
                        break;
                    case 'producto-transportista':
                        return config('psexport.cabeceras.v16.producto-transportista');
                        break;
                    case 'categoria':
                        return config('psexport.cabeceras.v16.categoria');
                        break;
                }

            }

        }

        if($movimiento=='baja')
        {
            return config('psexport.cabeceras.v16.producto_baja');
        }

        return false;

    }

    public function getContentsCsv($csv_file,$rows_array,$entidad,$movimiento)
    {
        if($entidad == 'categoria') {
            foreach ($rows_array as $row) {
                if(array_key_exists('url',$row))
                {
                    array_set($row, 'url', str_slug($row['name']));
                }
                $csv_file->insertOne($row);
            }
        }
        foreach ($rows_array as $row) {
            if($movimiento != 'baja')
            {
                /*
                                if($movimiento=='todo')
                                {
                                    if(array_key_exists('delete_images',$row))
                                        array_set($row, 'delete_images', '0');
                                }
                */
                if(array_key_exists('Reference',$row))
                {
                    array_set($row, 'Reference', str_pad($row['Reference'], 6, '0', STR_PAD_LEFT));
                }

                if(array_key_exists('Name',$row))
                {
                    $str_final = self::arreglarCaracteresEnTexto($row['Name']);
                    array_set($row, 'Name', $str_final);
                }

                if(array_key_exists('image_url',$row))
                {
                    $valor = $row['image_url'];

                    if(strpos($valor, '.jpg') !== false)
                    {
                        array_set($row, 'image_url', 'http://media.lancetahg.com.mx/p/'.$valor);

                    }
                    elseif (is_numeric($valor) )
                    {
                        $ruta = str_split((string)$valor);
                        array_set($row, 'image_url','https://www.lancetahg.com.mx/img/p/'.implode('/',$ruta).'/'.$valor.'.jpg');
                    }
                    else
                    {
                        array_set($row, 'image_url', '');
                    }

                }

                if(array_key_exists('Product_Reference',$row))
                {
                    array_set($row, 'Product_Reference', str_pad($row['Product_Reference'], 6, '0', STR_PAD_LEFT));
                }

            }
            $csv_file->insertOne($row);
        }

        return $csv_file;
    }

    public function csvName($entidad,$movimiento,$date)
    {
        $detalles_nombre = $this->_ps_version;

        $detalles_nombre .= $this->_con_atributos ? '_atrib' : '';

        if( ($entidad == 'marca' || $entidad == 'producto' || $entidad == 'combinacion') && $movimiento == 'modificacion')
        {
            return 'export'.$detalles_nombre.'_'. $entidad . '_'. $movimiento . '.csv';

        } else {

            return 'export'.$detalles_nombre.'_'. $entidad . '_'. $movimiento .'_' . $date . '.csv';

        }

    }

    protected function makeCsv($collection,$entidad,$movimiento,$date)
    {
        $collection_array = $collection->toArray();

        $csv_name = $this->csvName($entidad,$movimiento,$date);

        //ARCHIVO CSV PRODUCTOS
        $csv = Writer::createFromPath($this->getExportPath() . $entidad . '/'. $movimiento .'/' . $csv_name, "w");
        $csv->setDelimiter('|');

        //Inserta Nombre de columnas
        $csv->insertOne($this->getHeadersCsv($entidad,$movimiento));

        // Insertar los registros
        return $this->getContentsCsv($csv,$collection_array,$entidad,$movimiento);


    }

    public function recordExport($entidad,$movimiento,$csv_name,$ids,$user,$tiempo)
    {
        $array_ids = empty($ids) ? '' : $ids->implode(',');

        //Registro en BD
        $record = Export::create([
            'bd' => 'lanceta_bd_ec',
            'entidad' => $entidad,
            'tipo' => $movimiento,
            'tiempo' =>  '',
            'cron' => $user->name=='Cron' ? 1 : 0,
            'user_id'=> $user->name=='Cron' ? '' : $user->id,
            'archivo' => $csv_name,
            'ids_export' => $array_ids
        ]);

        return $record;
    }

    private function getFormatCountRecords($count)
    {
        return $count == 1 ? $count . ' registro' : $count . ' registros';
    }

    public function updateMarcasNuevas()
    {
        $title = 'Cambio automático en Marcas nuevas';
        $message = '<p style="text-align: center; text-transform: uppercase; font-weight: bold">';

        $model = 'Marca';

        $headers = Request::capture()->header();
        $uri_segments = Request::capture()->segments();
        $user = array_keys($uri_segments, 'updateMarcasNuevas') ? Auth::user() : (object)['name'=>'Cron'];

        $marcas_nuevas = Marca::whereNull('nombre')->whereNull('url')->get();

        try {


            if(count($marcas_nuevas)>0)
            {

                foreach($marcas_nuevas as $marca)
                {
                    $marca->update([
                        'nombre' => ucwords(strtolower($marca->nombre_upper)),
                        'url' => str_slug($marca->nombre_upper)
                    ]);
                }

            }

            $marcasactivadas = DB::affectingStatement("
					#Se desactivan marcas que no tengan productos asignados
					update marcas
					join (
						select m.id, m.nombre, m.nombre_upper, p.cant_prod, p.p_inactivos, m.activo as activo_old, if(p.cant_prod is
					null, 0, if(p.cant_prod = p_inactivos,0 ,1) ) as activo_new
						from marcas m
						left join (
							select
						productos.marca_id,
							count(*) as cant_prod,
							count(case when productos.activo = 0 then 1 end) as p_inactivos
							from productos
							group by marca_id
							order by marca_id
						) p on p.marca_id = m.id
					) mp on mp.id = marcas.id
					set marcas.activo = if(mp.activo_old <> mp.activo_new = 1, mp.activo_new,marcas.activo);
					");

            $success = true;
            $message .= ($marcasactivadas > 0) ? 'Se actualizó con éxito el estatus en ' . $marcasactivadas . ' marcas.</p>' : 'No hubo cambios en estatus de marcas.';
            $message .= '</p>';
            $message .= '<p style="text-align: center; text-transform: uppercase; font-weight: bold">' . (count($marcas_nuevas) > 0) ? 'Se actualizó con éxito el nombre y url de ' . count($marcas_nuevas) . ' marcas nuevas.</p>' : 'No hubo marcas nuevas.';
            $message .= '</p>';


        } catch(\Exception $e) {

            $success = false;
            $message .= 'No se realizaron las actualizaciones';
            $message .= '</p>';
            $message .= '<p><strong>Exception:</strong>' . $e->getMessage() . '</p>';

        }


        /*******************************************************
         * Enviar correo de notificación
         *******************************************************/
        $data_mail = [
            'title' =>  $title,
            'success'=>$success,
            'date' => Carbon::now()->format('d-m-Y H:i:s'),
            'marcas'	=>	$marcas_nuevas,
            'model'=> $model,
            'usuario' => $user->name,
            'user_id'=> $user->name=='Cron' ? '' : $user->id,
            'headers' =>    $headers,
            'mensaje' => $message,
//            'route' =>	(count($marcas_nuevas) > 0) ? 'http://admin.lancetahg.com.mx/marcas/' . $marcas_nuevas->id . '/edit' : ''
        ];
        $data = $data_mail;

        Mail::send('emails.update_marcas', $data , function($message) use ($data_mail)
        {
            $message->from(env('MAIL_FROM'));
            $message->to(env('MAIL_TO'))->subject('Notificación de tarea del cron');

        });


    }

    public static function arreglarCaracteresEnTexto($cadena)
    {
        return $cadena;
    }

}
