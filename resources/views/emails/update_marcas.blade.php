<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>

    <link href="/css/app.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<h3>{{ $title }}</h3>
<p>Fecha de la ejecución: {{ $date }}</p>
<p>Entidad: <br><strong>{{ isset($model) ? $model : 'Entidad' }}</strong></p>
<p>Usuario: <br><strong>{{ isset($usuario) ? $usuario : 'Usuario' }}</strong></p>
<hr>
<div class="alert alert-@if($success) 'success' @else 'danger' @endif" style="background-color: @if($success) #B1D2B1 @else #FF9999 @endif ;padding: 10px; margin-bottom: 30px">{!! $mensaje !!}</div>
@if(count($marcas)>0)
<table class="table table-bordered table-condensed">
    <tr>
        <td width="30">ID</td>
        <td width="80">Nombre LHG</td>
        <td width="80">Nombre Web</td>
        <td width="80">Url</td>
        <td width="100">Fecha de creación</td>
        <td width="50">Acción</td>
    </tr>
   @foreach($marcas as $marca)
    <tr>
        <td>{{ $marca->id }}</td>
        <td>{{ $marca->nombre_upper }}</td>
        <td>{{ $marca->nombre }}</td>
        <td>{{ $marca->url }}</td>
        <td>{{ $marca->created_at }}</td>
        <td>
            <a href="{{ 'http://admin.lancetahg.com.mx/marcas/' . $marca->id . '/edit' }}" style="padding: 7px; background-color: #1168BF; color: white ; font-size: 11px">Editar</a>
            {{--<a href="{{ action('MarcaController@edit', $marca->id) }}" style="padding: 7px; background-color: #1168BF; color: white ; font-size: 11px">Editar</a>--}}
        </td>
    </tr>
   @endforeach
</table>
@endif
</body>
</html>