<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $title }}</title>

        <link href="/css/app.css" rel="stylesheet">

        <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
        <body>
            <h3>{{ $title }}</h3>
            <div class="alert alert-success">La importación fue realizada con éxito</div>
            <p>Entidad: <br><strong>{{ isset($entidad) ? $entidad : 'Entidad' }}</strong></p>
            <p>Movimiento: <br><strong>{{ isset($movimiento) ? $movimiento : 'Movimiento' }}</strong></p>
            <p>Tiempo: <br><strong>{{ isset($tiempo) ? $tiempo : 'Tiempo' }}</strong></p>
            <p>Archivo: <br><strong>{{ isset($attach) ? $attach : 'Archivo adjunto' }}</strong></p>
            <p>No. de Registros: <br><strong>{{ isset($num_registros) ? $num_registros : 'No. de registros' }}</strong></p>
            <p>Usuario: <br><strong>{{ isset($usuario) ? $usuario : 'Usuario' }}</strong></p>
        </body>
</html>