<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="title">
                <h1 class="text-center">{{ $title }}</h1>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-toggle="collapse" data-target="#collapseForm" aria-controls="collapseForm"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(['route'=>['export.store'] ]) !!}
            <div class="modal-body">
                {!! Form::hidden('bd','lanceta_bd_ec') !!}

                <div class="form-group">
                    {!! Form::label('entidad', 'Entidad') !!}
                    {!! Form::select('entidad',
                    [
                        'marca'=>'Marcas / Todo',
                        'producto.alta'=>'Productos y Combinaciones / Altas',
                        'producto.modificacion'=>'Productos y Combinaciones / Modificaciones',
                        'producto.baja'=>'Productos y Combinaciones / Bajas',
                        'producto.todo'=>'Productos y Combinaciones / Todo',
                        'producto-transportista'=>'Productos-Transportistas / Todo',
                        'precio-especifico'=>'Precios específicos (Descuentos) / Todo',

                    ],'categoria',['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('cantidad', 'Tiempo Atrás',['style'=>'display:block']) !!}
                    {!! Form::number('cantidad', '1',['class'=>'form-control','style'=>'display:inline; width:auto']) !!}
                    {!! Form::select('periodo',
                        [
                            'i'=>'Minuto',
                            'h'=>'Hora',
                            'd'=>'Día',
                            'm'=>'Mes',
                            'Y'=>'Año'
                        ],'d',['class'=>'form-control','style'=>'display:inline; width:auto']) !!}
                </div>
{{--
                <div class="form-group col-md-6">
                    {!! Form::label('tipo', 'Tipo') !!}
                    {!! Form::select('tipo', ['todo'=>'Todo','alta'=>'Altas','modificacion'=>'Modificaciones'],'todo',['class'=>'form-control']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('consulta', 'Query') !!}
                    <textarea class="form-control" rows="6" id="consulta" name="consulta"></textarea>
                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('query_archivo', 'Archivo del query') !!}
                    {!! Form::text('query_archivo',null,['class' => 'form-control' , 'placeholder' => 'ejem. query_producto_export_todo_20170202_00000.sql']) !!}
                </div>
--}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#collapseForm" aria-controls="collapseForm">Cancelar</button>
                {!! Form::submit('Realizar',['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
            <script>
                $( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" }).val();
            </script>
        </div>
    </div>

</div>