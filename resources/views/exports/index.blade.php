@extends('layouts.dashboard')
@section('page_heading', $title )
@section('section')
    <div class="col-sm-12">
        <div class="container" style="border-bottom: 2px dashed #5c9ccc; width: 80%">
            <div class="row">
                <button class="btn btn-primary btn-sm update-export float-right" style="margin: 20px 0" id="btn-create" data-toggle="collapse" data-target="#collapseForm" aria-expanded="false" aria-controls="collapseForm" data-model="export" data-action="create">Agregar Exportación</button>

                <div id="collapseForm" class="form-container collapse" data-parent="btn-create" style="padding: 20px; background-color: #f4f4f4; margin: 0 0 20px 0;">
                    <img src="" alt="" id="loading">
                    {{--@include('exports.create')--}}
                </div>
            </div>
        </div>

        <div class="container">

            <div class="row">
                <table class="table table-stripped table-bordered">
                    <tr>
                        <td>Entidad</td>
                        <td>BD</td>
                        <td>Tipo</td>
                        <td>Hace (Tiempo)</td>
                        <td>Archivo</td>
                        <td>IDs export.</td>
                        <td>Realizada</td>
                    </tr>
                    @foreach($exports as $export)
                        <tr>
                            <td>{{ ucfirst($export->entidad) }}</td>
                            <td>{{ $export->bd }}</td>
                            <td>{{ $export->tipo }}</td>
                            <td>{{ $export->tiempo }}</td>
                            <td>{{ $export->archivo }}</td>
                            <td>{{ str_limit($export->ids_export,30) }}</td>
                            {{--<td>{{ date( 'F d, Y',strtotime($export->created_at) ) }}</td>--}}
                            <td>{{ $export->created_at }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>


        <div class="modal fade" id="Modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content form-content">

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>
@stop

@section('footer-scripts')
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
        var uri = '{{ url('') }}';

        $('#collapseForm').on('show.bs.collapse', function () {
            var button = $('#btn-create');
            button.addClass('disabled');

            $('#loading').show();

            var model = button.attr('data-model')
            var action = button.attr('data-action');
            var url = uri + '/' + model +'/' + action;

            $.ajax({
                url: url,
                cache: false,
                success: function (data) {
                    $('#collapseForm').html(data);
                }
            });


        })

        $('#collapseForm').on('hidden.bs.collapse', function () {
            $('#btn-create').removeClass('disabled');
            $('#collapseForm').data('');

        })

        $('.delete-item').on('click', function(e) {
            var id = $(this).data('id');
            var model = $(this).data('model');
            var row = $(this).closest('tr');
            var url = uri + '/' + model +'/' + id + '/delete';
            $.ajax({
                url:url,
                type: 'post',
                data: {_method: 'delete', _token: token},
                cache:false,
                success: function (data) {
                    row.fadeOut();
                }
            });
        })

//        $( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" }).val();
    </script>
@endsection