SELECT
        @pid := productos.id as id ,
        if(productos.activo = 1 ,1,0) as active,
        productos.nombre as name ,
        (
            select group_concat(categorias.nombre order by categorias.nivel desc separator ';')
            from categorias
            join categorias_productos cp on cp.categoria_id = categorias.id and cp.producto_id = @pid
        )as categories ,
        c.precio as price ,
        if(c.iva = 'N',2, 1) as tax_rule_id ,
        '' as wholesale_price ,
        '' as on_sale ,
        '' as discount_amount ,
        '' as discoun_percent ,
        '' as discoun_from ,
        '' as discoun_to ,
        c.id as reference ,
        c.modelo as supplier_reference ,
        left(c.id,3) as supplier ,
        ( select marcas.nombre from marcas where marcas.id = c.marca_id) as marca ,
        '' as ean ,
        '' as upc ,
        '' as ecotax ,
        c.ancho as width ,
        c.altura as height ,
        c.fondo as depth ,
        c.peso as weight ,
        e.exis_ec as quantity ,
        1 as minimal_quantity ,
        'both' as visibility ,
        '' as aditional_shipping_cost ,
        c.unidad as unity ,
        '' as unity_price ,
        '' as short_description ,
        productos.descripcion as description ,
        ( select @tags := group_concat(tags.nombre order by nombre separator ';') from tags where tags.producto_id = productos.id) as tags ,
        productos.nombre as meta_title ,
        @tags as meta_keywords ,
        '' as meta_description ,
        productos.url as url_written ,
        'Producto Disponible' as text_when_stock ,
        '' as text_when_backorder ,
        ( if
            (
                (
                select @dimensiones := count(case when (cd.peso > 0 and cd.altura > 0 and cd.ancho > 0 and cd.fondo > 0 ) then 1 end) as c_con_dim
                from codigos_dimensiones cd
                where cd.producto_id = @pid
                group by cd.producto_id
                ) >= 1
                and
                (
                    select @estafeta := cv.estafeta from codigos_ventaweb cv where cv.producto_id = @pid group by cv.producto_id
                ) = 1, 1 ,0
            )
        ) as available_for_order ,
        CURRENT_DATE() as available_date ,
        date(productos.created_at) as creation_date ,
        1 as show_price ,
        if( ( select @img_nom := i.nombre from imagenes i where i.producto_id = productos.id and i.deleted_at is null ) is not null, concat('http://media.lancetahg.com.mx/p/', @img_nom ) , '' ) as image_url ,
        1 as delete_images,
        if( (select @caracteristicas := group_concat(pe.nombre,':',pe.valor,':',pe.posicion order by posicion separator ';') from productos_especificaciones pe where pe.producto_id = productos.id ) is not null, @caracteristicas,'' ) as features ,
        '' as available_web_only ,
        '' as 'condition' ,
        '' as uploadable_files ,
        '' as text_fields ,
        if ( e.exis_total > 0 , 0 , 1 ) as out_of_stock ,
        1 as id_shop ,
        0 as advanced_stockManagement ,
        0 as depends_onStock ,
        0 as warehouse
FROM productos
join 
(
	select * from codigos where codigos.principal = 1 order by codigos.producto_id
) as c on c.producto_id = productos.id
join
(
    select existencia.id_producto,
    sum(existencia.total) as exis_total ,
    sum(existencia.ec) as exis_ec
    from existencia
    group by existencia.id_producto
) as e on e.id_producto = productos.id
where productos.deleted_at is null