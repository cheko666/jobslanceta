SELECT @idp := c.producto_id AS ID,
c.id AS Reference,
( GROUP_CONCAT(a.atributo,':',a.tipo,':',a.posicion ORDER BY posicion SEPARATOR ',') ) as Attribute,
( GROUP_CONCAT(a.valor,':',a.posicion ORDER BY posicion SEPARATOR ',') ) as 'Value',
c.modelo AS 'Supplier reference',
c.id AS 'Reference',
'' AS EAN13,
'' AS UPC, 
'' AS 'Wholesale price',
#Diferencia respecto al codigo de menor precio del grupo de productos
c.precio-cd.precio AS 'Impact on price',
'' AS Ecotax,
(select existencia.ec as exis_ec from existencia where existencia.id = c.id) AS Quantity,
1 AS 'Minimal quantity', 

#Diferencia respecto al codigo de menor peso del grupo de productos
if( cd.peso < c.peso ,round(c.peso-cd.peso,3),0) AS 'Impact on weight', 
c.principal AS 'Default', 
CAST(NOW() AS DATE) AS 'Combination available date', 
'' AS 'Image position',  
'' AS 'Image URL', 
0 AS 'Delete existing images', 
0 AS 'ID / Name of shop', 
0 AS 'Advanced Stock Managment', 
0 AS 'Depends on stock', 
0 AS 'Warehouse'
FROM codigos c
join productos p ON p.id = c.producto_id
left join (
	select * from codigos where codigos.principal = 1 
) cd on cd.producto_id = p.id
join (
    select *
	FROM atributos
 	WHERE atributos.atributo IS NOT NULL AND atributos.valor IS NOT NULL
) a ON a.codigo_id = c.id
WHERE c.producto_id is not null and p.deleted_at is null
#and p.id between 1200 and 1300
GROUP BY c.id