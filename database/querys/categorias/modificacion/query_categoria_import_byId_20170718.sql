SELECT a.id_category as id,
level_depth-1 as nivel,
id_parent as padre,
sa.position as orden,
name as nombre,
name as meta_keyword,
meta_title as meta_titulo,
description as descripcion,
b.link_rewrite as url,
'' as imagen,
a.date_add,
a.date_upd
FROM ps_category a
LEFT JOIN ps_category_lang b ON (b.id_category = a.id_category AND b.id_lang = 1 AND b.id_shop = 1)
LEFT JOIN ps_category_shop sa ON (a.id_category = sa.id_category AND sa.id_shop = 1)
WHERE 1 AND a.id_category = 332